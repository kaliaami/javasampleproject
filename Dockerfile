# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
#FROM alpine:latest

#CMD ["/bin/sh"]

FROM java:8
WORKDIR /
ADD ../src/JavaApplicatio/dist/SampleJava.jar /SampleJava.jar
CMD ["/usr/bin/java", "-jar",  "/SampleJava.jar"]
